# **Computer Aided Design**

## **Introduction:**

In this week, We are required to try out several softwares that are based on sketching and design. We then have to choose a software to sketch our project’s idea, The selection will be done on the basis of the most suitable software. The softwares include the following models ( Raster, Vector, 2D, 3D, Render, Animation, Simulation). We are also required to provide the final sketch of our project. After testing the software, We need to note down every step that we did in order to execute the sketch.

## **Software I used:**

- [Libreoffice](https://www.libreoffice.org/discover/draw/)
- [Fusion 360](https://www.autodesk.com/products/fusion-360/free-trial)
- [Inkscape](https://inkscape.org/)


## **Apps:**

- [Magisto](https://www.magisto.com/?utm_source=google&utm_medium=ppc&utm_campaign=Brand&utm_term=magisto&matchtype=p&distribution=search&ad_id=384770705473&placement=&adgroup=Brand_1_phrase&gclid=EAIaIQobChMIlq3pk9_G5wIVksjeCh3HYAvhEAAYASAAEgKrDvD_BwE)

- [Bazaart](https://apps.apple.com/us/app/bazaart-photo-editor-design/id515094775)

## **Software:**

### **Libreoffice:**

Let me start by talking about the easiest software that I’ve dealt with which is Libreoffice draw. The drawings that I made with this software were a house, sun, 3 flowers and 2 clouds as you can see in the figure below. In order to control the location of the component that I inserted I had to choose (select), then after that I’m able to change the colors of the line and the fill to anything that suits me and by doing so my drawing is ready. I really enjoyed this software since it was very easy to deal with and all the required components are there so if you’re thinking about something just select it and it will be ready. My drawing was inspired by the only thing that I was able to draw back during the school days so that’s why I decided to go with it.

![](img/CAD1.PNG)


### **Inkscape:**

The second software that I used is called inkscape. This software transforms the pictures into vectors and it has many other features and uses. I searched the internet and found a picture of a penguin so I saved it and did the following commands:
- File > Import > Select the Image > Open > Select the Image then go to path > Bitmap trace: Unselect all the options except for the brightness step > click update.

![](img/CAD2.PNG)


- After performing these commands the picture will become black and white, So I had to press ok and after that 2 pictures popped up, A vector and the old picture, Then I had to delete the old picture and clicked ok to ensure that the picture is turned into a vector.

![](img/CAD3.PNG)


### **Fusion 360:**

The third software that I used was fusion 360 which is similar to AutoCAD, this software is easy to use and have many features which make it the best option to design our final project with it. But before designing the final project I want to test this software by drawing a game boy ( 3D Design) so let me start with the steps that I followed in order to create a 3D design for the game boy:

1) I Draw a rectangle

 ![](img/CAD4.PNG)

 2) I clicked on the Extrude option to extrude the rectangle

 ![](img/CAD5.PNG)

 3) Then I draw another rectangle and this rectangle will be the screen of the game boy

  ![](img/CAD6.PNG)

  4) And again I extrude the  new rectangle

  ![](img/CAD7.PNG)

  5) Then I  returned the object to the top view and draw a 4 small circles and extrude them, these circles will represent the buttons for the game boy  

  ![](img/CAD8.PNG)

  6) Again I draw a 2 intersection rectangles and extrude them

  ![](img/CAD9.PNG)   

  7) And Finally draw 2 small rectangle and extrude them, and here is the final sketch

  ![](img/CAD10.PNG)     


### **Sketch of the Final Project:**

The software that I used to execute my final project design is fusion360 which has many great features and is very easy to use. I’ll write down every step that I did to execute this sketch.

1. I drew two different circles that had unique diameters and I made sure that they are intersecting each other.

  ![](img/CAD11.PNG)   

2. Now I had to remove the parts that are intersecting between the two circles.

  ![](img/CAD12.PNG)   

3. Now the edges are sharp but I don’t want them to be so, that’s why I used the fillet option which turned the sharp edges to round.

  ![](img/CAD13.PNG)   

4. I used the offset option in order to copy my object and enlarge it, So now I ended up with two drawings that have the same shape but have different sizes.

  ![](img/CAD14.PNG)   

5. I drew a rectangle which will represent the area surrounding the swimming pool.

  ![](img/CAD15.PNG)     

6. I clicked on the parts that I wanted to be higher and then clicked on the extrude option to execute the command.

  ![](img/CAD16.PNG)      

7. I wanted to ensure that the swimming pool had a certain depth and I can add water to it later on so I used the shell option which adds a depth to the pool.

  ![](img/CAD17.PNG)   

8. I clicked on the pool then I right clicked and selected the appearance option and the library will show up, Then I’ll choose the water folder which has many types of water and then I chose the swimming pool water and dragged it to the pool in my drawing.

  ![](img/CAD18.PNG)  

9. This is how the drawing looks like after I added the water to it.

  ![](img/CAD19.PNG)  

10. As for the area surrounding the swimming pool I did the same exact steps mentioned earlier is steps 8&9 but instead of choosing the water folder, I chose the wood folder since I wanted the area surrounding the swimming pool to be wooden and I kept adding the wooden layer to the pool’s surrounding. This is how my drawing looks after adding the wooden layer to it.

![](img/CAD20.PNG)  

11. At the end I had to ensure that my drawing represents reality, So I clicked on the render option then selected In-canvas render. This option keeps on generating iterations to ensure that my drawing realistic and as the number of iterations increase the drawing will look more realistic, I stopped at 207 iterations.

![](img/CAD21.PNG)  

12. This this how the final drawing look like

![](img/CAD22.PNG)


## **Apps:**

I want to start things off by saying that I’m a person who enjoys using phone apps more than computer softwares and I usually find them to be dealt with easily that’s why I preferred using the phone applications to make a video and to edit some photos. The phone apps can be accessed at any time that suits me and wherever I may be unlike the computer softwares that I used which required me to access my laptop and then try to run the software which I found to be somewhat tedious whereas for the mobile apps I felt comfortable.

I’m going to list down all the applications that I used along with a brief introduction about each application and then I’m going to write down the steps.


### **Magisto:**

This application allows the user to create a video and has many great features and it’s very easy to use.

1.  I launched the application and then selected the pictures that I wanted to include within my video.

![](img/CAD23.PNG)

2. After that a new page pops up which contains the editing styles. The application offers many styles so I chose the one that suits me the most.

![](img/CAD26.PNG)

3. Then I was asked to choose the type of music that I wanted to add to my video directly from library that was provided within the app itself or I can download a music from my phone’s library and if I wanted there was an option to keep the video without any songs.

![](img/CAD24.PNG)

4.After that the final option will show up which asks me to add a title to my video and decide the video’s length and there was an option to edit the video if I changed my mind at the last moments which I found to be very convenient and helpful.

![](img/CAD25.PNG)

5. After I’m done I clicked ok and the app started working to finish up my video and it was ready in a few seconds.

[Video link](https://www.magisto.com/video/MlkfZF9bFiohX01iCzE?o=i&c=e&l=mmr1&tp=AgMCXjUmPFZIV1xbWHc9CBIFCQwOKjpbRV9dDl94Ow9IBQgKWHxtW1cUCUkFOioIFDkFXlc5MQ8UCTNIDy48ElcTH18YEDEPTFdbC1l8bFJDUEpZAi42BRQKUV8HLjEH)


### **Bazaart**

I downloaded this application previously for the purpose of making WhatsApp stickers but the thing that I didn’t know about is that this application has many great features. I knew about these features after watcching a short video that explains the application from the apple store so I really liked the application and I decided to edit my photo with a space background because I like the outer space. This application has many great backgrounds and it will let you ask your self twice if you wanted to choose a background ainply because all of them are attractive.

1. I opened the application, then I clicked on the add option and chose the space background.

![](img/CAD27.PNG)

2.  I clicked in photo, then I selected a photo from my phone library so that I can merge it with the background.

![](img/CAD28.PNG)

3.I clicked on the photo that I added and then chose the edit option then the eraser option and I kept erasing all the parts that weren’t necessary which left me with the part that I wanted to merge with the background.

![](img/CAD29.PNG)

4. I clicked on the add option once more and added a text to my photo.

![](img/CAD30.PNG)

5. I clicked on the picture once more and selected the edit option after that I selected the blending option then the overlay option.

![](img/CAD31.PNG)

6. I enlarged the photo and changed the text’s location and this is how the picture looks like after editing. I’m really impressed with what I did because the editing was on point.

![](img/CAD32.PNG)

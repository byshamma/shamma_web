# **Application and Implication**

## **Introducation**

In this week we are required to answers some questions related to our final project and as usual I am the last minutes person who decide the final project idea in the last day. I had many ideas on my mind and I was confused which one will be better. The ideas were farming robots, heating table and machine that remove the sand from the fence of the road. After taking my instructor and my colleagues opinions I decided to choose the heating table. So let me now answer the following questions.

## **Questions**

**1. What will it do?**

This tray table will contain a heater which will start heating the food when the sensor detect that there is an object on the table. I will used the electricity to provide me with the power I need for heating. The idea comes to my mind since I am a person who spent the most of my time taking a pictures of my food so the food becomes cold so the taste of the food becomes worse. I was thinking about something that I can do to solve this problem so the heating table comes to my mind.

**2. Who's done what beforehand?**

There is a Japanese invention called **kotatsu** which is a low, wooden table frame covered by a futon, or heavy blanket, upon which a table top sits. Underneath is a heat source, formerly a charcoal brazier but now electric. But my idea is different than this. my table will heat the food and the table is  tray table


**3. What will you design?**

- I will design a tray table itself
- The circuit board needed to program the table
- Handles for the table
- stickers or logo for the table

**4. What materials and components will be used?**

-  Wood : The tray table will be made from wood (Nonmetal) for the safety purpose.

-  Iron or any metal: There will be a small area on the table which will be made of metal which allow the table to heat the object. This material will be covered by the wood when we reach the result or the desirable heat temperature which will be safer for the person and it will not make the table view bad. I am caring about the design of the table because its important.

**5. Where will come from?**

I think all the materials are available in our lab

**6. How much will they cost?**

I think it will not cost more than **200 AED**


**7. What parts and systems will be made?**

????

**8. What processes will be used?**

- For the table 3D design I am going to use fusion 360 and I will use the ShopBot cnc machine to cut the parts needed to make the tray table

- For the circuit board I will use Eagle for the design and the Roland cnc milling machine for the milling process.

- I will use the 3D printer to make handles for my table

- I will use the vinyl cutting machine to add stickers to my table.


**9. What questions need to be answered?**


**10. How will it be evaluated?**

it will be evaluated based on its ability to heat the food to a desirable temperature.

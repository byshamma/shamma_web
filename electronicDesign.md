# **Electronic Design**

## **Introduction**
In this week we were required to design a circuit board using a software called eagle. Personally speaking this is my second time using this software, my first was during the winter camp but to be honest I didn’t get familiar with it back then and I felt that I had to use this software more if I wanted to truly know the ins and outs. So this week I faced some issues while trying to design the circuit board but I want to give some credits to my colleagues since they helped me a lot and they ensured that if I had any concern they’ll help me with it, and thankfully I was able to finish the circuit board on time. I was under stress since we were a few days away from the deadline and I was a bit late, but in the end I was able to finish it on time and during the submission date I performed the milling operation on the circuit board and then I soldered it. All of this has been done in a good time and it really teaches me a lesson and that is to always finish my work on time and never postpone any work just before the deadline by a couple of days


### **Steps Required to Design the ATTiny 44:**

Now I’m going to explain the steps that I did to design the Attiny 44

**Step 1:**  I opened eagle software and saved the file under a specific name then I opened the schematic page and  chose add port:

![](img/w38.PNG)

**Step 2:** I added all the components that I required to design the circuit board from the fab library. This option was already included within the software. Below are the name of the files:

1. ```Attiny 44```

2. ```ISP Header```

3. ```10K ohm Resistor```

4. ```499 ohm Resistor```

5. ```20 MHz Resonator```

6. ```FTDI Connector```

7. ```1 uF Capacitor```

8. ```Push Button```

9. ```LED```

**Step 3:** I connected the components as shown in the screenshot below

![](img/w39.PNG)

**Step 4:** I then changed the page that I was on from schematic to board page

![](img/w40.PNG)

**Step 5:**  I used the airwire tool to do the connection again and arrange the components in a way to ensure that there will be no intersection between any two lines

![](img/w41.PNG)

**Step 6:** I clicked on DRC and changed all the clearance values to 17 because the drill bit for the milling the traces is (1\64) which is equal to 0.0157 to be exact.

![](img/w42.PNG)

**Step 7:** This is the final shape of the circuit board.

![](img/w45.PNG)

**Step 8:** I clicked on layer settings and then clicked on hide for all the layers except the top layer

![](img/w46.PNG)

**Step 9:**  Then I had to save the picture so I followed the steps that are shown in the screenshots below.

![](img/w44.PNG)

**Step 10:** For the outline I used paint software to make the outline and this is result of the outline and trace

![](img/w47.PNG)

**Step 11:** I opened fabmodulus software and followed the steps that were mentioned during the [electronic production](https://byshamma.gitlab.io/shamma_web/week2d.html) week then I had to save them on a flash in order to use them on the computer that is connected to the milling machine.

**Step 12:** Then I had to print the circuit board, So I repeated the steps that I did during the [electronic production](https://byshamma.gitlab.io/shamma_web/week2d.html) and this is how the circuit board looks after executing the milling operation

![](img/w48.PNG)

**Step 13:** I brought the components that I required to perform the soldering operation and I stuck them together on a paper so that I don’t lose them and I’ll be able to find them easily when required and I wrote the name of each component on the paper.

![](img/w49.PNG)

**Step 14:** This is how the circuit board looks like after the soldering process

![](img/w50.PNG)


### **Steps Required to Design the atmega 328:**

The second circuit board that I designed was atmega 328 which is similar to Arduino Uno board but the difference between them is that The Arduino is a complete system (Hardware with supporting components), and software. The atmega 328 is a Microprocessor that needs external components, and a programmer to operate.

I'm going the explain the steps that I did in order to design the atmega 328:

1. I opened the [atmega](http://academy.cba.mit.edu/classes/embedded_programming/hello.arduino.328P.png) board that neil designed and then I opened eagle and add all the components that neil added then I think of what should I add more for my final project so what I added is listed below:

- 2 button (Input)

- LED (output)

- Servo motor (output)

- Potentiometer (input)

2. This this how the schematic board look likes after adding all the components:

![](img/ed1.PNG)

3. Then I had to move to the layout page which was the hardest part and the part that takes days from me. This is how the layout looks like :

![](img/ed2.PNG)

4. Then instead of using ``fabmodulus`` or ``Mods`` our instructor explain for us a new software which is easier and take less time and effort than ``fabmodulus``. This software called ``flatCAM``.

5. To move to ``flatCAM`` I clicked on Manufacturing tool >> CAM to open the cam processor, generate and export the Gerber files.

![](img/ed3.PNG)

6. Then I moved to ``flatCAM`` and open the Gerber files ( the traces and the outline ) and the Excellon files (for the drill parts) as shown below:

![](img/ed4.PNG)

![](img/ed5.PNG)

7. 

# **Final Project**

## **Project Idea:**

The idea of my projects is about a smart swimming pool which has a built in sensor a few meters away from it whenever this sensor is switched on, If it detects anyone coming near the pool it will raise a net that will ensure that no child will fall into the swimming pool if they were not guarded which will significantly reduce the number of accidents that occur and will make the parents feel safer.

## **Initial Sketch:**

![](img/sketch.PNG)

### **Sketch of the Final Project:**

The software that I used to execute my final project design is fusion360 which has many great features and is very easy to use. I’ll write down every step that I did to execute this sketch.

1. I drew two different circles that had unique diameters and I made sure that they are intersecting each other.

  ![](img/CAD11.PNG)   

2. Now I had to remove the parts that are intersecting between the two circles.

  ![](img/CAD12.PNG)   

3. Now the edges are sharp but I don’t want them to be so, that’s why I used the fillet option which turned the sharp edges to round.

  ![](img/CAD13.PNG)   

4. I used the offset option in order to copy my object and enlarge it, So now I ended up with two drawings that have the same shape but have different sizes.

  ![](img/CAD14.PNG)   

5. I drew a rectangle which will represent the area surrounding the swimming pool.

  ![](img/CAD15.PNG)     

6. I clicked on the parts that I wanted to be higher and then clicked on the extrude option to execute the command.

  ![](img/CAD16.PNG)      

7. I wanted to ensure that the swimming pool had a certain depth and I can add water to it later on so I used the shell option which adds a depth to the pool.

  ![](img/CAD17.PNG)   

8. I clicked on the pool then I right clicked and selected the appearance option and the library will show up, Then I’ll choose the water folder which has many types of water and then I chose the swimming pool water and dragged it to the pool in my drawing.

  ![](img/CAD18.PNG)  

9. This is how the drawing looks like after I added the water to it.

  ![](img/CAD19.PNG)  

10. As for the area surrounding the swimming pool I did the same exact steps mentioned earlier is steps 8&9 but instead of choosing the water folder, I chose the wood folder since I wanted the area surrounding the swimming pool to be wooden and I kept adding the wooden layer to the pool’s surrounding. This is how my drawing looks after adding the wooden layer to it.

![](img/CAD20.PNG)  

11. At the end I had to ensure that my drawing represents reality, So I clicked on the render option then selected In-canvas render. This option keeps on generating iterations to ensure that my drawing realistic and as the number of iterations increase the drawing will look more realistic, I stopped at 207 iterations.

![](img/CAD21.PNG)  

12. This this how the final drawing look like

![](img/CAD22.PNG)

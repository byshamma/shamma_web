# **Input Devices**

## **Introduction:**

In this week we are required to add one or more sensor to our microcontroller board that we designed and read it. Since our lab is closed because of the coronavirus I wasn't able to finish my board because I did the same mistake that I did it before which is postpending the work until the last day. So instead of using my own circuit board I used a simulation software called **TinkerCAD**. I really enjoyed using this software. It is easy to use and there are a lot of tutorials on how to deal with it. I recommend it.

## **What is Input Device?**

An **input device** is a piece of hardware used to provide data to a computer used for interaction and control.  It allows input of raw data to the computer for processing.

## **What is TinkerCAD**

**TinkerCAD** is a browser-based 3D design and modeling program created to provide a way for a variety of users (beginners to experts) to create projects.

Conventional CAD software options are not only expensive, but they’re also often quite complicated to learn. These programs often have many features, that you won’t even use for something as simple as a custom case. While they are great for professional users, makers will more likely be happy with TinkerCAD. It’s not only free but also very easy to learn and to use.


## **Get Started**

I'm going to list down all the steps that I did in order to read different sensors on TinkerCAD

-  I opened the [TinkerCAD website](https://www.tinkercad.com/dashboard) and created an account to access the services.

- I selected **Circuits** on the left side of the screen:

 ![](img/inp1.PNG)

- Then I selected **Create new Circuit** on the next page and the below screen will appear

  ![](img/inp2.PNG)

  To familiarize myself with the circuit design homepage, I go over the areas I've marked with numbers:

  1. Change the name of your design here
  2. This is the main toolbar. From the left to the right: Rotate the selected part clockwise, delete the selected part, undo and redo your last action(s), create annotations and toggle their visibility.
  3. Some parts allow you to change their program. Click here to do so.
  4. Start the simulation
  5. Export your design to Autodesk EAGLE.
  6. Choose the parts to display in the list below. I recommend choosing “All”.
  7. These are the parts you can use.
  8. The main work area. Drag and drop parts here to add them to your design.
  9. Fit everything into the view.



- After I get familiar with the website I start searching about the sensors that I can used with Arduino. It can be found in this page:

[Sensors](https://www.elprocus.com/arduino-sensor-types-and-applications/)

## **Ultrasonic Sensor**

- The first sensor that I read was [ultrasonic sensor](http://wiki.seeedstudio.com/Ultra_Sonic_range_measurement_module/). It detects the distance of the closest object in front of the sensor (from 3 cm up to 400 cm). It works by sending out a burst of ultrasound and listening for the echo when it bounces off of an object. It pings the obstacles with ultrasound. The Arduino or Genuino board sends a short pulse to trigger the detection, then listens for a pulse on the same pin using the ``pulseIn()`` function. The duration of this second pulse is equal to the time taken by the ultrasound to travel to the object and back to the sensor. Using the speed of sound, this time can be converted to distance.

- I added the ultrasonic sensor to the Arduino and I connect them together as its shown below

  ![](img/inp3.PNG)

- Then I added the code. The code was taken from [Arduino website](https://www.arduino.cc/en/tutorial/ping)

- Then I clicked on ``start simulation`` button to start the operation.

- I clicked on the circle below which represent the object and change the position of the object from the sensor. At that time the serial monitor value was changing based on the change that I made.

  ![](img/inp4.PNG)
- And here are some of the serial monitor values:

  ![](img/inp5.PNG)

## **PIR Sensor**

A passive infrared sensor is an electronic sensor that measures infrared light radiating from objects in its field of view. They are most often used in PIR-based motion detectors. PIR sensors are commonly used in security alarms and automatic lighting applications.

- I added the components to my page and make the connection between them as shown below

  ![](img/inp6.PNG)

- I used the below code for programming:

````
int led = 6;                // the pin that the LED is atteched to
int sensor = 3;              // the pin that the sensor is atteched to
int state = LOW;             // by default, no motion detected

void setup() {
  pinMode(led, OUTPUT);      // initalize LED as an output
  pinMode(sensor, INPUT);    // initialize sensor as an input
  Serial.begin(9600);        // initialize serial
}

void loop(){
  int pir = digitalRead(sensor);   
  if (pir == HIGH) {           // check if the sensor is HIGH
    digitalWrite(led, HIGH);   // turn LED ON
    Serial.print(" LED ON and value = ");
     Serial.println(pir);
    delay(100);                // delay 100 milliseconds

  }
  else {
     digitalWrite(led, LOW);   // turn LED ON
    Serial.print(" LED OFF and value = ");
     Serial.println(pir);
    delay(100);                // delay 100 milliseconds
  }
}

````


- here is the serial monitor values

  ![](img/inp7.PNG)

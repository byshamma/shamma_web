## **Project Idea:**

The idea of my projects is about a smart swimming pool which has a built in sensor a few meters away from it whenever this sensor is switched on, If it detects anyone coming near the pool it will raise a net that will ensure that no child will fall into the swimming pool if they were not guarded which will significantly reduce the number of accidents that occur and will make the parents feel safer.

## **Initial Sketch:**

![](img/sketch.PNG)

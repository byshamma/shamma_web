# **Week1: Project Management**

## **Introduction**

To start things off, I didn’t have that huge background in HTML coding since we had a brief overview of the language at university so when I heard that I have to create my own website I said to myself “Well this is going to be hard and I won’t think I’ll be able to accomplish such task” but here I am proud of what I was able to come up with and actually I never could’ve that creating a website will be that easy so if anyone is wondering if you can do it or not trust me you can do it and you’ll have fun doing so

## 1) Defining the software and websites that I used:
In this week of the fab academy we learned to use many software's, each software has it’s benefit and will help me in developing my own website and they’ll help me in my documentation as well. I’m going to list down each software with a brief description of it.

- **Git bash:** Is an application that uses specific commands to apply changes to our websites.

- **Brackets:** Is a software that focuses mainly on web development and it’s used for editing source codes as well. It’s main purpose is strictly tied to the live editing functionality of the HTML, CSS and JavaScript coding.

- **GitLab:** Is a service that provides the Git repositories a remote access. It also works as a host to anyone’s code. It also helps in providing extra features that are purely designed to help in managing software development lifecycle. Some of the additional features include managing code sharing between different people and devices.

- **Bootstrap:** Is considered to be the most popular CSS framework for websites that are responsive and mobile-first.

- **Atom:** Is a software that is developed by GitHub. It is an open source text editor and it provides us with a platform that will help in creating an interactive and responsive web application.

## 2) installing and Creating an Account:
 - Installing [Git Bash](https://git-scm.com/download/win) software

 - Creating an account on [GitLab](https://about.gitlab.com/) then configuring my account using Git Bash and write the following commands:

 ![](img/week1/config.PNG)

## 3) Generating SSH key:

 The purpose of generating a ssh is to make a direct connection between the computer and Gitlab

 - open the git bash and write the following commands:

  ![](img/week1/ssh.PNG)

- ssh key will generate in my computer:

![](img/week1/uss.PNG)

- I copied the ssh key to gitlab >> setting >> add key:

![](img/week1/addk.PNG)

- ssh key is active now

## 4) Cloning My Project:

- We received an email from fab academy coordination to access the Fab Academy 2020 Archive.

- we supposed to go to [GitLab](https://gitlab.fabcloud.org/) and reset our password using the same email that we used when we resister for fab academy.

- we logged in to our pages and we add the ssh key. After that we downloaded all the defaults files which included the website that we can use it so we tried it.

- Our instructor explained for us how use this website using markdown language. And here are the steps that we did to test this website:

1- We followed the [mkdocs](https://www.mkdocs.org/) tutorial.

2- In order to manually install MkDocs we need [Python](https://www.python.org/) installed and python manager [pip](https://pip.readthedocs.io/en/stable/installing/) then we check if they already installed from the command line:

```
$ python --version
Python 2.7.2
$ pip --version
pip 1.5.2
```

3-  I installed the Mkdocs package using the following command ```pip install mkdocs```

4- I wrote ``` mkdocs serve``` to generate a live preview link of the web and I start making changes but honestly I didn't like the website design so I decided to stay with the web that I created so I start removing the default using this command ```git rm -r file name .file extension```. and replacing them with my website files.

- I wrote the following command to clone my project:

![](img/week1/clone.PNG)

- Automatically a new file will generate in the desktop under the name of my project name and it will include a git file  

![](img/week1/ff.PNG)


## 5) Creating a Website:

- At the very beginning when I wanted to design my own website I used another template from a website called [bootstrap](https://startbootstrap.com/themes/creative/) but I didn’t feel that it was suitable when I viewed other people’s websites since theirs was way better than my template so I wanted to look for a template that really suits me and in the same time others will find it very attractive so I came across [Salama’s website](http://fab.academany.org/2018/labs/fablabuae/students/salama-altamimy/) who is a previous Fab academy student and I really liked her template so I decided to choose it as my own and down below you can see my previous and new templates You’ll definitely notice the difference between the two where the new template is way nicer and arranged.

![](img/week1/oldweb.PNG)

- I put the file on my folder **"Shamma_Web"**

- I download [Brackets](http://brackets.io/)

- I opened the index file using brackets and also i opened the live preview to see the changes at the same time.

![](img/week1/bb.PNG)

- I Modified the template using brackets.

- Now I need to follow some steps using Git Bash to add changes, pull, commit and push the files. This can be done using the following commands:

```
git pull
git add --all
git commit -m "message"
git push
```

- After pushing all the files to my Gitlab account. I need now to create a link for my website. To do this I should go to my Gitlab account and click on CI/CD configuration. In order to build my plain HTML site with GitLab Pages. I need to write the following code:

![](img/week1/cc.PNG)

- And Here is My [website Link](https://byshamma.gitlab.io/shamma_web/)

## 6) Documenting My Work:

We are required to document every step that we perform in detail while developing the website. Furthermore, we are required to document our work on a weekly basis and upload it to our website. I didn’t feel that HTML was the ideal program the document my steps so I asked my colleagues and they told me that they are using atom software to document their steps which uses a language called markdown that is easier than HTML’s. So I opened a [tutorial](https://www.markdowntutorial.com)  and learned about this language and used this software to document all my steps and then I had to change the language from markdown to HTML so I downloaded a program called Pandoc and it helped me transfer the file directly from markdown to HTML and this was done by the following command:

````
pandoc filename.md -f markdown -t html -c filename.css -s -o filename.html
````


## 7) Problems that I Faced:
I faced several issues while developing my website, The first being that I saved the files in a wrong folder and when I tried to push them they were not uploading to the website and then I realized that I had to save the files within the folder that has the .git file. The second issue that I faced was regarding the SSH Key, because I changed its location and I placed it in another folder so it wasn’t working, I had to do a new SSH Key and I kept it in the same location and it worked. The third issue was that I was getting an error which is shown in the screenshot below, I tried searching in google to know how I can resolve it and I found that the only thing to do to solve this problem is to write this command ````git pull````

![](img/week1/w10.PNG)

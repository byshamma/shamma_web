# **Electronic Production**
## **Introduction**
At the start of this week we were required to produce an ISP chip by using the CNC milling machine, solder the components as required and finally we were required to program the ISP. To start things off, I want to note out that I did not have any background regarding this area so I was dealing with this thing for the first time in my life, and when I tried to solder the components I failed several times until I was finally able to do it correctly. But hey! I won't say that I didn't have fun doing so, the experience by itself was so entertaining and I felt that every time I was making progress, the most difficult part was to program the ISP chip since I didn't attend the lecture that week due to the weather conditions on that day so I was a bit late and I didn't know where to start but thankfully my colleagues helped me and I was able to finish programming the chip. If there is anything that I want to recommend, You should not skip a lecture because the work you will have to put in will be twice or three times the effort needed if you attended the lecture itself

## **Group Assignment**

## **CNC Milling Machine Test**

##  **What is required?**

- Test the CNC milling machine using the 0.2 (0.010) milling pit.

## **What We did?**

- Installed the [line test](http://academy.cba.mit.edu/classes/electronics_production/linetest.png) picture from the fab academy website to use it for our test.

## **The steps that we follow in order to test the milling pit:**

1. Open the [Mods](http://mods.cba.mit.edu/) page and follow the [tutorial](https://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/mods.html) mentioned in the fab academy page.

2. Right click on the page and chose ```program```

![](img/cnc1.png)

3. After that a new screen will appear and we will choose ```open server program```

![](img/cnc2.PNG)

4. We need to select the machine and its type so our machine is ```Roland mill (SRM 20)``` so I chose it as shown below

![](img/cnc3.PNG)

5. The new screen will appear where I required to add a ```png file```

![](img/cnc4.PNG)

6. I clicked on ```mill traces (1/64)```

![](img/cnc5.PNG)

7. I changed the tool diameter to be ```0.2 mm``` then clicked ```calculate``` and the below picture will appear

![](img/cnc6.PNG)

8. I need to delete the input that is connected with the output so I clicked on ```delete```

![](img/cnc7.PNG)

9. Right Clicked on the page and chose ```modules```

![](img/cnc8.PNG)

10. Then I chose open server modules >> file save. Then the new input will appear so we connect it with the output as shown below

![](img/cnc9.PNG)

11. I recalculated the process and then the file will be saved automatically on my computer.

12. I fixed the xyz points on the roland machine and make sure they are in the right place after that I entered the file and clicked on cut to start the milling process and what happened is when the machine start working it goes up or to be more specific it goes somewhere not the xyz points that I chose so it started milling on the air. At the beginning I thought that I miss a step or I did something wrong so I tried to print it again but the same problem occurs so I asked for help from our instructor and again the same problem happened. So he told me to try to use fab modules instead of mods so when I follow the fab modules steps it worked and the machine work perfectly and this was the result of using the ```0.2 mm``` milling pits:

![](img/cnc10.PNG)

Here is the Link of the group [Assignment page](http://academany.fabcloud.io/fabacademy/2020/labs/uae/site/eproduction/epro.html)

## **Steps required to perform the milling operation on the ISP and cutting it:**

1. The [traces](http://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/fts_mini_traces.png) PNG files and the board [outline](http://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/fts_mini_cut.png) has to be downloaded.

2. Open the [Fab Modules]((http://fabmodules.org/) and then insert the image -> Output (Roland Mill) -> Process -> PCB traces (1/64) -> Machine (SRM20).


3. Now you should change the settings of the XYZ axis to Zero as shown in the figure below.

![](img/w1.PNG)

4. After that click on the calculate button and you’ll end up with the figure below.

![](img/w2.PNG)

5. The previous steps should be repeated for the board outline but instead of selecting the traces, this time around we’ll select outlines (1/32) and we’ll end up with what is shown in the figure below.

![](img/w4.PNG)

6. After we’re done, We have to save the two pictures using the PSB in order to use them with the computer that is connected directly to the CNC machine.

7. Now we should fix the “FR1” plate on the MDF by using a double sided tape.

 ![](img/w3.PNG)

8. The figure below represents a MDF that is fixed on the CNC machine.

 ![](img/w5.PNG)

9. We have to fix the milling pit inside the machine.

10. Now we have to control the directions of the XYZ-axis coordinates which can be done by using ```VPanel software```.

11. After setting the XYZ coordinates, We have to ensure that the XY-axis coordinates aren’t touched where as the Z-axis should be changed depending to ensure that the milling pit touches the MDF.


12. Now go to cut -> Delete all -> Add traces -> Output.

  ![](img/w7.PNG)

13. The same steps will be repeated for the board outline, but we have to change the milling pit to (1/32) this time.

14. Now the circuit board is ready and is shown in the figure below.

 ![](img/w6.PNG)

### **Mods**

The newest ```FABMODULES``` is called ```MODS```. The``` MODS ```is more capable than the previous modules and works by connecting the nodes to develop each process. The workflows within the MODS can be customized as one wishes by loading several modules at one time or by using a precompiled program.

I used the MODS to perform the CNC milling machine test, by inserting the PNG picture for the line test and performed the necessary changes and then saved the ```stl``` file in order to use it in the CNC milling machine. But whenever I start up the process the milling pit lifts up into the air which causes the machine to start the milling process in the air. I tried the same process on 3 separate occasions but I keep ending up with the same issue so I decided to use the FABMODULES to do the line test for the CNC milling machine.


## **Steps Required to Soler The ISP**
1) I used tape to stick the components because of their tiny size

 ![](img/w9.PNG)

2) I used iron soldering to solder the components

 ![](img/w8.PNG)

3) Now I need to check if there is any short circuit or any place that need more solder so I used millimeter for checking

![](img/w11.PNG)

4) The circuit board is ready now for programming

## **Steps required to Program the ISP:**


1. I followed the [tutorial](http://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/windows_avr.html) and installed all the required program which are:

- [AVR toolchain](https://www.microchip.com/)

- [AVRdude](http://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/avrdude-win-64bit.zip)

- [GNU make](http://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/make-3.81.exe)

2. Now I have to update the path so the following steps were executed:

Go to system properties -> Environment variable -> Path -> Add the bath as described in the tutorial.

![](img/w15.PNG)

3. The following commands should be written in order to ensure that I installed them all and to check the path.

![](img/w16.PNG)

4. I connected Engineer Hashim’s FabISP (Programmed ISP) with my ISP and then installed the Zadig driver and clicked install.

5. Now I have to download the [firmware](http://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/fts_firmware_bdm_v1.zip) and extract the files.


6. Open the git bash on the firmware folder and write down the following commands:

- ```make flash``` The following command erases the target chip, and it will program the flash memory with the contents of the.hex file that was previously built.

- ```make fuses``` The following command will set up all the fuses excluding the fuse that will disable the reset pin.

- ```make rstdisbl``` The following command works in a way that is very similar to the make fuses command, but this command includes an extra feature which is thhe reset disable feature.


7. The ISP is programmed


## **Problems I Faced:**
There were several problems that I faced while working this week.

- The first one was that I entered the milling pit much more that what was actually needed so this resulted in digging the circuit much more than what was necessary and it broke down so I had to repeat the work once more and print it again.

![](img/w12.PNG)

- The second issue is that the soldering of the first circuit was much more than the ideal amount required so it resulted in a short circuit and I wasn't able to resolve this issue so I had to build a new circuit board and do the soldering one more time.

![](img/w13.PNG)

- The third and last issue that I faced is that I didn't insert the ribbon cable into the connector in a correct way so when I was trying to connect the two circuits the light wasn't turning on and I resolved this issue by hammering the ribbon cables till they attached correctly to the connector
